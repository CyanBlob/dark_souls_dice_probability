import random, sys
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

black  = [2, 2, 1, 1, 1, 0]
blue   = [2, 1, 2, 1, 2, 3]
orange = [3, 1, 3, 2, 2, 4]
green  = [0, 0, 0, 1, 1, 1]

numTests = 100000

def rollDice(color):
   return color[random.randint(0, 5)]

def main():
    fullRolls = [] # all rolls from all runs
    for _ in range(0, numTests):
        rolls = []
        for i in range(1, len(sys.argv), 2):
            color = black
            count = int(sys.argv[i])
            if sys.argv[i + 1] == "blue":
                color = blue
            elif sys.argv[i + 1] == "orange":
                color = orange
            elif sys.argv[i + 1] == "green":
                color = green
            for j in range(0, count):
                rolls.append(rollDice(color))
        fullRolls.append(rolls)

    roll_freq = [] # frequency of each roll
    labels = ["0"] # label for each bar (percentages)
    # count occurances of each possible value
    for i in range(0, numTests):
        _sum = 0
        # sum up each set of rolls
        for j in range(0, len(fullRolls[i])):
            _sum += fullRolls[i][j]
        # increase arrays if necessary
        while _sum > len(roll_freq) - 1:
            roll_freq.append(0)
            labels.append(str(int(labels[-1]) + 1))
        # store occurances of each possible value
        roll_freq[_sum] = roll_freq[_sum] + 1

    freq_series = pd.Series(np.array(roll_freq))
    plt.figure(figsize=(12, 10))
    ax = freq_series.plot(kind='bar')
    # format arguments nicely to create chart title
    ax.set_title(" ".join(sys.argv[1:]))
    ax.set_xlabel('Value')
    ax.set_ylabel('Frequency')
    ax.set_xticklabels(labels[0:-1])

    rects = ax.patches

    labels = []
    percent_remaining = 100.0
    for i in range(0, len(roll_freq)):
        if roll_freq[i] == 0:
            labels.append("")
        else:
            labels.append(f"{(roll_freq[i] / numTests) * 100:.2f}%\n({percent_remaining:.2f}%)")
            percent_remaining -= (roll_freq[i] / numTests) * 100

    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height + 10, label,
                ha='center', va='bottom')
    plt.show()

if __name__ == "__main__":
    main()
