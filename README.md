# Dark_Souls_dice_probability

This is a simple Python script to calculate the dice roll statistics in the Dark Souls board game, since it uses non-standard dice

## Dependencies
Matplotlib (`pip install matplotlib`)

Pandas (`pip install pandas`)

Numpy (`pip install numpy`)


Note: You may need to use `pip3` if `pip` still refers to `pip` 2.7 on your system

## Usage:

`python roll_tester.py 2 black 1 blue 1 orange`
